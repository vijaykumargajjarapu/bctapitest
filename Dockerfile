# Use the base Python image
FROM python:3.9-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the requirements file to the working directory
COPY requirements.txt .

# Install the required Python packages
RUN pip3 install --no-cache-dir -r requirements.txt

# Copy the test files to the working directory
COPY com.bct.testcases/*.py/ tests/

# Run pytest when the container starts
CMD ["pytest","-v", "/app/tests/rateMovie.py"]