import requests

def test_rateMovie_statusCode200():
    #GET - Retrieve Guest Session ID
    guestResponse = requests.get("https://api.themoviedb.org/3/authentication/guest_session/new?api_key=40159f96f23ab6d5cb9565b1dc70f383")
    assert guestResponse.status_code == 200
    guestResponseBody = guestResponse.json()
    guestSessionId = guestResponseBody["guest_session_id"]

    #GET - Retrieve Movie ID
    getMovieIdResponse = requests.get("https://api.themoviedb.org/3/movie/top_rated?api_key=40159f96f23ab6d5cb9565b1dc70f383&language=en-US&page=1")
    getMovieIdResponseBody = getMovieIdResponse.json();
    assert getMovieIdResponse.status_code == 200
    movieID = getMovieIdResponseBody["results"][1]["id"]

    #Post - Rate a Movie
    rateMovieResponse = requests.post("https://api.themoviedb.org/3/movie/"+str(movieID)+"/rating?api_key=40159f96f23ab6d5cb9565b1dc70f383&guest_session_id="+str(guestSessionId),
                                      {
                                          "value": 8.5
                                      }
                                      )
    assert rateMovieResponse.status_code == 201
    rateMovieResponseBody = rateMovieResponse.json()
    assert rateMovieResponseBody["status_message"] == "Success."

def test_rateMovie_statusCode401():
    #GET - Retrieve Guest Session ID
    guestResponse = requests.get("https://api.themoviedb.org/3/authentication/guest_session/new?api_key=40159f96f23ab6d5cb9565b1dc70f383")
    assert guestResponse.status_code == 200
    guestResponseBody = guestResponse.json()
    guestSessionId = guestResponseBody["guest_session_id"]

    #GET - Retrieve Movie ID
    getMovieIdResponse = requests.get("https://api.themoviedb.org/3/movie/top_rated?api_key=40159f96f23ab6d5cb9565b1dc70f383&language=en-US&page=1")
    getMovieIdResponseBody = getMovieIdResponse.json();
    assert getMovieIdResponse.status_code == 200
    movieID = getMovieIdResponseBody["results"][2]["id"]

    #Post - Rate a Movie
    rateMovieResponse = requests.post("https://api.themoviedb.org/3/movie/"+str(movieID)+"/rating?api_key=40159f96f23ab6d5cb9565b1dc70f3&guest_session_id="+str(guestSessionId),
                                      {
                                          "value": 8.5
                                      }
                                      )
    assert rateMovieResponse.status_code == 401
    rateMovieResponseBody = rateMovieResponse.json()
    assert rateMovieResponseBody["status_message"] == "Invalid API key: You must be granted a valid key."

def test_rateMovie_statusCode404():
    #GET - Retrieve Guest Session ID
    guestResponse = requests.get("https://api.themoviedb.org/3/authentication/guest_session/new?api_key=40159f96f23ab6d5cb9565b1dc70f383")
    assert guestResponse.status_code == 200
    guestResponseBody = guestResponse.json()
    guestSessionId = guestResponseBody["guest_session_id"]

    #GET - Retrieve Movie ID
    getMovieIdResponse = requests.get("https://api.themoviedb.org/3/movie/top_rated?api_key=40159f96f23ab6d5cb9565b1dc70f383&language=en-US&page=1")
    getMovieIdResponseBody = getMovieIdResponse.json();
    assert getMovieIdResponse.status_code == 200
    movieID = getMovieIdResponseBody["results"][2]["id"]

    #Post - Rate a Movie
    rateMovieResponse = requests.post("https://api.themoviedb.org/3/mov/"+str(movieID)+"/rating?api_key=40159f96f23ab6d5cb9565b1dc70f383&guest_session_id="+str(guestSessionId),
                                      {
                                          "value": 8.5
                                      }
                                      )
    assert rateMovieResponse.status_code == 404
    rateMovieResponseBody = rateMovieResponse.json()
    assert rateMovieResponseBody["status_message"] == "The resource you requested could not be found."