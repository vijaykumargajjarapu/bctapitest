import pytest
import requests

def test_getTopRatedMoviesList_statusCode200():
    getTopRatedMovieResponse = requests.get(
        "https://api.themoviedb.org/3/movie/top_rated?api_key=40159f96f23ab6d5cb9565b1dc70f383&language=en-US&page=1")
    assert getTopRatedMovieResponse.status_code == 200;


def test_getTopRatedMoviesList_statusCode401():
    getTopRatedMovieResponse = requests.get(
        "https://api.themoviedb.org/3/movie/top_rated?api_key=40159f96f23ab6d5cb9565b1dc70f3&language=en-US&page=1")
    assert getTopRatedMovieResponse.status_code == 401;
    getTopRatedMovieResponseBody = getTopRatedMovieResponse.json()
    assert getTopRatedMovieResponseBody["status_message"] == "Invalid API key: You must be granted a valid key."


def test_getTopRatedMoviesList_statusCode404():
    getTopRatedMovieResponse = requests.get(
        "https://api.themoviedb.org/3/movie/topated?api_key=40159f96f23ab6d5cb9565b1dc70f383&language=en-US&page=1")
    assert getTopRatedMovieResponse.status_code == 404;
    getTopRatedMovieResponseBody = getTopRatedMovieResponse.json()
    assert getTopRatedMovieResponseBody["status_message"] == "The resource you requested could not be found."
